<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

class _EventBaseClass_ClassNameTransformation
{
    public function getHashCode()
    {
        return "_EventBaseClass_ClassNameTransformation";
    }

    public function __invoke($object)
    {
        return $object->data->_className;
    }
}

class _EventBasedClass_ClassNameCondition
{
    private $classes;

    public function __construct($classes)
    {
        if (!is_array($classes))
            $classes = array($classes);
        $this->classes = $classes;
    }

    public function __invoke($className)
    {
        foreach ($this->classes as $class)
            if (strpos($className, $class) === false)
                return false;
        return true;
    }
}

class EventBasedClass
{
    public static function getClassParameterConstraint($parameterName, $supportedClassTypes)
    {
        static $classNameTransformation = null;
        if ($classNameTransformation === null)
            $classNameTransformation = new _EventBaseClass_ClassNameTransformation();

        $condition = new _EventBasedClass_ClassNameCondition($supportedClassTypes);

        return new ParameterConstraint($parameterName, $condition, $classNameTransformation);
    }

    private $eventRegistry;
    private $mainEventBus;
    public $data;

    public function __construct($eventRegistry, $mainEventBus, $className)
    {
        $this->eventRegistry = $eventRegistry;
        $this->mainEventbus = $mainEventBus;

        $this->data = (object)array("_className" => $className);
    }

    public function __call($name, $arguments)
    {
        $i = count($arguments);
        $eventArgs = ($i > 0) ? (object)$arguments[0] : new \stdClass();
        $options = ($i > 1) ? $arguments[1] : array();

        $eventArgs->self = $this;
        $eventArgs->eventBus = $this->mainEventBus;
        $eventArgs->eventName = $name;
        
        return Handler\HandlerChain::processEvent($name, $this->eventRegistry, $eventArgs, $options);
    }
}
