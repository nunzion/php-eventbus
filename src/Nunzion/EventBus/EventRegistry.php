<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

use Nunzion\Expect;
use Nunzion\Select;
use Nunzion\ArrayHelper;

class EventRegistry
{
    public function registerHandler(array $handlerDescription, $handler)
    {
        Expect::that($handlerDescription)->itsArrayElement("handledEvent")->isDefined();
        if (!is_callable($handlerDescription["handledEvent"]) && !is_string($handlerDescription["handledEvent"]))
            Expect::that($handlerDescription)->itsArrayElement("handledEvent")->_("must be callable or type of 'string'");
        Expect::that($handlerDescription)->itsArrayElement("priority")->isUndefinedOrInt();
        Expect::that($handlerDescription)->itsArrayElement("constraints")->isUndefinedOrArray();
        Expect::that($handlerDescription)->itsArrayElement("inheritedArguments")->isUndefinedOrArray();
        
        $internalHandler = new \stdClass();
        $internalHandler->handler      = $handler;
        $internalHandler->handledEvent = $handlerDescription["handledEvent"];
        $internalHandler->priority     = ArrayHelper::tryGetNotNullValue($handlerDescription, "priority", 100); //Default Priority
        $constraints = ArrayHelper::tryGetValue($handlerDescription, "constraints");
        if ($constraints !== null)
            for ($i = 0; $i < count($constraints); $i++)
                Expect::that($handlerDescription)->itsArrayElement("constraints")->itsArrayElement($i)->isInstanceOf("\Nunzion\EventBus\ParameterConstraint");
        $internalHandler->constraints = $constraints;
        
        $this->internalHandlers[] = $internalHandler;
    }

    private $internalHandlers;
    private $eventNameGroupedInternalHandlers;
    private $fullEventHashGroupedHandlers;

    private $eventNameGroupedIndices;


    private static function matches($pattern, $subject)
    {
        return is_callable($pattern) ?
            $pattern($subject) : $pattern === $subject;
    }

    public function getHandlers($eventName, $eventArgs = null)
    {
        if ($eventArgs === null)
        {
            $internalEventHandlers = &$this->eventNameGroupedInternalHandlers[$eventName];
            if ($internalEventHandlers === null)
            {
                $internalEventHandlers = array_filter($this->internalHandlers,
                    function ($internalHandler) use ($eventName) {
                        return self::matches($internalHandler->handledEvent, $eventName); });
            }
            return $internalEventHandlers;
        }
        else
        {
            //<editor-fold defaultstate="collapsed" desc="build $eventIndices">

            $eventIndices = &$this->eventNameGroupedIndices[$eventName];

            if (!$eventIndices)
            {
                $eventIndices = array();
                foreach ($this->getHandlers($eventName) as $internalHandler)
                    if ($internalHandler->constraints !== null)
                        foreach ($internalHandler->constraints as $constraint)
                        {
                            $valueTransformation = $constraint->getTransformation();
                            $valueTransformationId = $valueTransformation === null ? "" : $valueTransformation->getHashCode();
                            $constraintTypeId = $constraint->getParameterName() . ":" . $valueTransformationId;
                            if (!isset($eventIndices[$constraintTypeId]))
                                $eventIndices[$constraintTypeId] =
                                    array($constraint->getParameterName(), $valueTransformation);
                        }
            }

            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="build $constraints and $hash including eventName and arguments">

            $eventIndicesHash = "";
            foreach ($eventIndices as $index)
            {
                $parameterName = $index[0];
                if (property_exists($eventArgs, $parameterName))
                {
                    $valueTransformation = $index[1];
                    $transformedValue = ($valueTransformation) ?
                        $valueTransformation($eventArgs->$parameterName) :
                        $eventArgs->$parameterName;
                    $eventIndicesHash .= chr(0) . $transformedValue;
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="build $handlers">

            $fullMatchingHandlers = &$this->fullEventHashGroupedHandlers[$eventName . $eventIndicesHash];
            if (!$fullMatchingHandlers)
            {
                $parameterNameGroupedRequestHashes = array();
                foreach ($eventIndices as $index)
                {
                    $parameterName = $index[0];
                    if (property_exists($eventArgs, $parameterName))
                    {
                        $valueTransformation = $index[1];
                        $transformedValue = ($valueTransformation) ?
                            $valueTransformation($eventArgs->$parameterName) :
                            $eventArgs->$parameterName;

                        $parameterNameGroupedRequestHashes[$parameterName][] = $transformedValue;
                    }
                }

                $fullMatchingInternalHandlers = array();

                foreach ($this->getHandlers($eventName) as $internalHandler)
                {
                    $isHandlerAllowed = ArrayHelper::all($internalHandler->constraints,
                            function($constraint) use ($parameterNameGroupedRequestHashes) { //all constraints have to be true
                                $requestHashes = ArrayHelper::tryGetValue($parameterNameGroupedRequestHashes, $constraint->getParameterName());

                                return $requestHashes !== null && //test: === null ||
                                    ArrayHelper::any($requestHashes, function ($requestHash) use ($constraint) {
                                        return self::matches($constraint->getCondition(), $requestHash);
                                    });
                    });

                    if ($isHandlerAllowed)
                        $fullMatchingInternalHandlers[] = $internalHandler;
                }

                usort($fullMatchingInternalHandlers, 
                    function($a, $b) 
                    { 
                        return strnatcmp($a->priority, $b->priority); 
                    });
                        
                $fullMatchingHandlers = array_map(Select::member("handler"), $fullMatchingInternalHandlers);
            }

            //</editor-fold>

            return $fullMatchingHandlers;
        }
    }
}
