<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

class EventBus
{
    private $eventRegistry;

    public function __construct($eventRegistry)
    {
        $this->eventRegistry = $eventRegistry;
    }

    public function __call($name, $arguments)
    {
        $i = count($arguments);
        $eventArgs = ($i > 0) ? (object)$arguments[0] : new \stdClass();
        $options = ($i > 1) ? $arguments[1] : array();

        $eventArgs->eventBus = $this;
        $eventArgs->eventName = $name;

        return Handler\HandlerChain::processEvent($name, $this->eventRegistry, 
                $eventArgs, $options);
    }
}
