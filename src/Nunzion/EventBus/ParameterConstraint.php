<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

use Nunzion\Expect;

class ParameterConstraint
{
    public function __construct($parameterName, $condition, $transformation = null)
    {
        Expect::that($parameterName)->isString();
        Expect::that($transformation)->isNullOrCallable();

        $this->parameterName = $parameterName;
        $this->condition = $condition;
        $this->transformation = $transformation;
    }

    private $parameterName;
    public function getParameterName()
    {
        return $this->parameterName;
    }

    private $condition;
    public function getCondition()
    {
        return $this->condition;
    }

    private $transformation;
    public function getTransformation()
    {
        return $this->transformation;
    }
}
