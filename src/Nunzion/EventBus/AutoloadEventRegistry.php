<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

class AutoloadEventRegistry
{
    private $baseEventRegistry;
    private $classes;
    
    public function __construct($baseEventRegistry)
    {
        $this->baseEventRegistry = $baseEventRegistry;
        
        $that = $this;
        spl_autoload_register(function ($className) use ($that)
            {
                $classHandler = @$that->classes["\\" . $className];
                if ($classHandler !== null)
                    $classHandler->load();
            });
    }
    
    public function registerHandler($handlerDescription, $handler)
    {
        if ($handler instanceof Handler\ClassHandler)
            $this->classes[$handler->getClassName()] = $handler;
        
        return $this->baseEventRegistry->registerHandler($handlerDescription, $handler);
    }
    
    public function getHandlers($eventName, $eventArgs = null)
    {
        return $this->baseEventRegistry->getHandlers($eventName, $eventArgs);
    }
}
