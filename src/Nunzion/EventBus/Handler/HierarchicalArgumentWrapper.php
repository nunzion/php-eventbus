<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

class HierarchicalArgumentWrapper
{
    private $___parent;
    
    public function __construct($parent)
    {
        $this->___parent = $parent;
    }
    
    public function getParent()
    {
        return $this->___parent;
    }
    
    function __isset($name)
    {
        if ($this->___parent !== null)
            return isset($this->___parent->$name);
        else
            return false;
    }
    
    public function __get($name)
    {
        if ($this->___parent !== null)
            $result = $this->___parent->$name;
        else
            throw new \Exception("Argument does not exist.");
         
        $this->$name = $result;
        return $result;
    }
}
