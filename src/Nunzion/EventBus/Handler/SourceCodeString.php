<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

use Nunzion\Expect;

class SourceCodeString implements SourceCodeInterface
{
    private $source;
    private $isLoaded;

    public function __construct($sourceString)
    {
        Expect::that($sourceString)->isString();
        $this->source = $sourceString;
    }

    public function load()
    {
        if (!$this->isLoaded)
        {
            $this->isLoaded = true;
            eval($this->source);
        }
    }
}
