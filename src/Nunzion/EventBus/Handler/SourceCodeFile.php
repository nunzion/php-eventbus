<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

class SourceCodeFile implements SourceCodeInterface
{
    private $sourceFile;
    private $isLoaded;

    public function __construct($sourceFile)
    {
        $this->sourceFile = $sourceFile;

        if (!file_exists($sourceFile))
            throw new \Exception();
    }

    public function load()
    {
        if (!$this->isLoaded)
        {
            $this->isLoaded = true;
            require_once $this->sourceFile;
        }
    }
}
