<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

use Nunzion\Expect;

abstract class BaseHandler
{
    private $source;
    private $loaded = false;
    private $additionalArgs;

    public function __construct(SourceCodeInterface $source = null)
    {
        $this->source = $source;
    }

    public function setAdditionalArgs($args)
    {
        Expect::that($args)->isNullOrObject();
        $this->additionalArgs = $args;
    }

    public function getAdditionalArgs()
    {
        return $this->additionalArgs;
    }

    protected function prepareArgs($args)
    {
        if ($this->additionalArgs === null)
            return $args;

        $newArgs = clone $args;
        foreach ($this->additionalArgs as $name => $value)
            $newArgs->$name = $value;

        return $newArgs;
    }

    protected function load()
    {
        if (!$this->loaded)
        {
            if ($this->source !== null)
                $this->source->load();
            $this->loaded = true;
        }
    }

    abstract function __invoke($eventArgs, $handlerChain);
}
