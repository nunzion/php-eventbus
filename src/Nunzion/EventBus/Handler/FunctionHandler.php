<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

use Nunzion\Expect;

class FunctionHandler extends BaseHandler
{
    private $function;

    public function __construct($function, SourceCodeInterface $source = null)
    {
        parent::__construct($source);

        Expect::that($function)->isString();
        $this->function = $function;
    }

    public function __invoke($args, $handlerChain)
    {
        $this->load();
        $args = $this->prepareArgs($args);
        $function = $this->function;
        return $function($args, $handlerChain);
    }
}
