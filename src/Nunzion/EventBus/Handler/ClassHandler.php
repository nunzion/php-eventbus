<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

use Nunzion\Expect;

class ClassHandler extends BaseHandler
{
    private $className;

    public function __construct($className, SourceCodeInterface $source = null)
    {
        parent::__construct($source);

        Expect::that($className)->isString();
        $this->className = $className;
    }

    public function __invoke($args, $handlerChain)
    {
        $this->load();
        $args = $this->prepareArgs($args);
        $className = $this->className;
        return new $className($args, $handlerChain);
    }

    public function load()
    {
        parent::load();
    }

    public function getClassName()
    {
        return $this->className;
    }
}
