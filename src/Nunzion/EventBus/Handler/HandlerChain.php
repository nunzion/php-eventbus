<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

class HandlerChain
{
    private static $currentArgumentWrapper;
    
    public static function processEvent($eventName, $eventRegistry, 
            $arguments, array $options = array())
    {
        $parent = self::$currentArgumentWrapper;
        $argumentWrapper = new HierarchicalArgumentWrapper($parent);
        foreach ($arguments as $name => $value)
            $argumentWrapper->$name = $value;
        self::$currentArgumentWrapper = $argumentWrapper;
        
        $handlers = $eventRegistry->getHandlers($eventName, $argumentWrapper);
        $chain = new HandlerChain($handlers);
        $result = $chain->next($argumentWrapper, $options);
        
        self::$currentArgumentWrapper = $parent;
        
        return $result;
    }
    
    public static function processHandlers($handlers,
            $arguments, array $options = array())
    {
        $argumentWrapper = new HierarchicalArgumentWrapper(
                $arguments, self::$currentArgumentWrapper);
        self::$currentArgumentWrapper = $argumentWrapper;
        
        $chain = new HandlerChain($handlers);
        $result = $chain->next($argumentWrapper, $options);
        
        self::$currentArgumentWrapper = self::$currentArgumentWrapper->getParent();
        
        return $result;
    }
        
    public function __construct(array $handlers)
    {
        $this->handlers = $handlers;
        $this->currentHandler = -1;
        $this->handlersMax = count($this->handlers) - 1;
    }

    private $handlers;
    private $currentHandler;
    private $handlersMax;
    
    public function next($arguments, array $options = array())
    {
        if ($this->currentHandler < $this->handlersMax)
        {
            $handler = $this->handlers[++$this->currentHandler];

            $result = $handler($arguments, $this);

            --$this->currentHandler;

            return $result;
        }
        elseif (array_key_exists("defaultResult", $options))
            return $options["defaultResult"];
        else
            throw new \Exception("No handler.");
    }

    public function hasNext()
    {
        return $this->currentHandler < $this->handlersMax;
    }
}
