<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

use Nunzion\RegExp as RegExp;


class EventRegistryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var EventRegistry
     */
    protected $eventRegistry;


    protected function setUp()
    {
        $this->eventRegistry = new EventRegistry();

        $this->eventRegistry->registerHandler(array("priority" => 100, "handledEvent" => "FooEvent"), "MyHandler2");

        $this->eventRegistry->registerHandler(array("priority" => 99, "handledEvent" => "FooEvent"), "MyHandler1");

        $this->eventRegistry->registerHandler(array("priority" => 101, "handledEvent" => new RegExp("/Ev.nt/")), "MyHandler3");

        $this->eventRegistry->registerHandler(array("priority" => 102, "handledEvent" => new RegExp("/Event/"),
                  "constraints" => array(new ParameterConstraint("foo", new RegExp("/b/")))), "MyHandler4");

        $this->eventRegistry->registerHandler(array("priority" => 103, "handledEvent" => new RegExp("/Event/"),
                  "constraints" => array(new ParameterConstraint("foo", "improbable"))), "MyHandler5");

        $this->eventRegistry->registerHandler(array("priority" => 104, "handledEvent" => new RegExp("/Event/"),
                  "constraints" => array(new ParameterConstraint("foo", "improbable"),
                                         new ParameterConstraint("foo2", new RegExp("/llo/")))), "MyHandler6");
    }


    public function testGetHandlers()
    {
        for ($i = 0; $i < 3; $i++)
        {
            $this->assertEquals(array('MyHandler1', 'MyHandler2', 'MyHandler3'),
                    $this->eventRegistry->getHandlers("FooEvent", (object)array("foo" => "ar")));

            $this->assertEquals(array('MyHandler1', 'MyHandler2', 'MyHandler3', 'MyHandler4'),
                    $this->eventRegistry->getHandlers("FooEvent", (object)array("foo" => "bar")));

            $this->assertEquals(array('MyHandler1', 'MyHandler2', 'MyHandler3', 'MyHandler4'),
                    $this->eventRegistry->getHandlers("FooEvent", (object)array("foo" => "improbable2")));

            $this->assertEquals(array('MyHandler1', 'MyHandler2', 'MyHandler3', 'MyHandler4', 'MyHandler5'),
                    $this->eventRegistry->getHandlers("FooEvent", (object)array("foo" => "improbable")));

            $this->assertEquals(array('MyHandler3', 'MyHandler4', 'MyHandler5'),
                    $this->eventRegistry->getHandlers("FoEvent", (object)array("foo" => "improbable")));

            $this->assertEquals(array('MyHandler3'),
                    $this->eventRegistry->getHandlers("FoEvant", (object)array("foo" => "improbable")));

            $this->assertEquals(array('MyHandler3', 'MyHandler4', 'MyHandler5', 'MyHandler6'),
                    $this->eventRegistry->getHandlers("FoEvent", (object)array("foo" => "improbable", "foo2" => "Hello")));
        }
    }

    public function testSpecialHandlers()
    {
        $mockupTransformation1 = new GreaterThanTransformation(5);
        $mockupTransformation2 = new GreaterThanTransformation(4);

        $this->eventRegistry->registerHandler(array("priority" => 103, "handledEvent" => "special",
                  "constraints" => array(new ParameterConstraint("foo", true, $mockupTransformation1))), "MySpecialHandler1");

        $this->eventRegistry->registerHandler(array("priority" => 104, "handledEvent" => "special",
                  "constraints" => array(new ParameterConstraint("foo", false, $mockupTransformation2))), "MySpecialHandler2");

        $this->eventRegistry->registerHandler(array("priority" => 104, "handledEvent" => "special",
                  "constraints" => array(new ParameterConstraint("foo", false, $mockupTransformation1))), "MySpecialHandler3");

        $this->eventRegistry->registerHandler(array("priority" => 104, "handledEvent" => "special",
                  "constraints" => array(new ParameterConstraint("foo", false, $mockupTransformation1))), "MySpecialHandler4");

        $this->assertEquals(array('MySpecialHandler1'),
                $this->eventRegistry->getHandlers("special", (object)array("foo" => 6)));
        $this->assertEquals(array('MySpecialHandler4', 'MySpecialHandler3', 'MySpecialHandler2'),
                $this->eventRegistry->getHandlers("special", (object)array("foo" => 4)));
        $this->assertEquals(array('MySpecialHandler4', 'MySpecialHandler3', 'MySpecialHandler2'),
                $this->eventRegistry->getHandlers("special", (object)array("foo" => 3)));
        $this->assertEquals(array('MySpecialHandler1'),
                $this->eventRegistry->getHandlers("special", (object)array("foo" => 7)));

        $this->assertEquals(6, $mockupTransformation1->getInvokeCount());
        $this->assertEquals(6, $mockupTransformation2->getInvokeCount());
        $this->assertEquals(3, $mockupTransformation1->getHashCodeCallCount());
        $this->assertEquals(1, $mockupTransformation2->getHashCodeCallCount());
    }
}


class GreaterThanTransformation
{
    public function __construct($other)
    {
        $this->other = $other;
    }

    private $other;

    public function getHashCode()
    {
        $this->hashCodeCallCount++;
        return "GreaterThan:" . $this->other;
    }

    public function __invoke($input)
    {
        $this->invokeCount++;
        if (!is_int($input))
            return false;
        return $input > $this->other;
    }

    private $invokeCount = 0;

    public function getInvokeCount()
    {
        return $this->invokeCount;
    }

    private $hashCodeCallCount = 0;

    public function getHashCodeCallCount()
    {
        return $this->hashCodeCallCount;
    }
}
