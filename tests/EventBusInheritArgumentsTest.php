<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

use Nunzion\RegExp as RegExp;

class EventBusInheritArgumentsTest extends \PHPUnit_Framework_TestCase 
{
    public function testInheritedArguments()
    {
        $eventRegistry = new EventRegistry();

        
        $eventRegistry->registerHandler(array("handledEvent" => "load"),
                function ($args) { 
                    return $args->eventBus->handleRequest(array("request" => $args->input));
                });
                
        $eventRegistry->registerHandler(array("handledEvent" => "handleRequest"), 
                function ($args) {
                    return $args->eventBus->doSth();
                });
 
        $eventRegistry->registerHandler(array("handledEvent" => "doSth"), 
                function ($args) {  
                    return $args->request . ":doSth";
                });
                
        $eventBus = new EventBus($eventRegistry);    
        

        $this->assertEquals("Hallo:doSth", $eventBus->load(array("input" => "Hallo")));
        $this->assertEquals("Test:doSth", $eventBus->load(array("input" => "Test")));
    }
    
    
    public function testInheritedArgumentConstraints()
    {
        $eventRegistry = new EventRegistry();

        
        $eventRegistry->registerHandler(array("handledEvent" => "load"),
                function ($args) { 
                    return $args->eventBus->handleRequest(array("request" => $args->input));
                });
                
        $eventRegistry->registerHandler(array("handledEvent" => "handleRequest"), 
                function ($args) {
                    return $args->eventBus->doSth(null, array("defaultResult" => "empty"));
                });

        $eventRegistry->registerHandler(array("handledEvent" => "doSth", "constraints" => array(
                      new ParameterConstraint("request", new RegExp("/H/")))),
                function ($args) {  
                    return $args->request . ":doSth2";
                });
                
        $eventBus = new EventBus($eventRegistry);    
        

        $this->assertEquals("Hallo:doSth2", $eventBus->load(array("input" => "Hallo")));
        $this->assertEquals("Hi:doSth2", $eventBus->load(array("input" => "Hi")));
        $this->assertEquals("empty", $eventBus->load(array("input" => "Test")));
    }
}
