<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;


class ChainOrderTestHandler
{
    private $position;

    public function __construct($position)
    {
        $this->position = $position;
    }

    public function __invoke($args, $handlerChain)
    {
        return $this->position . ($handlerChain->hasNext() ? $handlerChain->next($args) : "");
    }
}

class HandlerChainTest extends \PHPUnit_Framework_TestCase
{

    public function testChainOrder()
    {
        $handlers = array(new ChainOrderTestHandler(0), new ChainOrderTestHandler(1), new ChainOrderTestHandler(2), new ChainOrderTestHandler(3));
        $this->assertEquals("0123", HandlerChain::processHandlers($handlers, new \stdClass()));
    }

    /**
     * @expectedException \Exception
     */
    public function testException()
    {
        $f = function ($args, $handlerChain) { return $handlerChain->next($args) + 1; };
        $handlers = array($f, $f, $f, $f);
        HandlerChain::processEvent($handlers, new \stdClass());
    }

    public function testDefaultResult()
    {
        $f = function ($args, $handlerChain) { return $handlerChain->next($args, array("defaultResult" => 0)) + 1; };
        $handlers = array($f, $f, $f, $f);
        $this->assertEquals(4, HandlerChain::processHandlers($handlers, new \stdClass()));
    }

    public function testMultipleNext()
    {
        $f = function ($args, $handlerChain) { return $handlerChain->next($args, array("defaultResult" => 1)) + $handlerChain->next($args, array("defaultResult" => 1)); };
        $handlers = array($f, $f, $f);
        $this->assertEquals(8, HandlerChain::processHandlers($handlers, new \stdClass()));
    }
}
