<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

class EventBasedClassTest extends \PHPUnit_Framework_TestCase
{
    public function testEventBasedClass()
    {
        $eventRegistry = new EventRegistry();


        $eventRegistry->registerHandler(array("handledEvent" => "setName", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Animal_"))),
                function ($args) { $args->self->data->_name = $args->value; });

        $eventRegistry->registerHandler(array("handledEvent" => "getName", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Animal_"))),
                function ($args) { return $args->self->data->_name; });



        $eventRegistry->registerHandler(array("handledEvent" => "greet", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Cat_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Human_"))),
                function () { return "Miau"; });

        $eventRegistry->registerHandler(array("handledEvent" => "greet", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Human_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Animal_"))),
                function ($args) { return "Hello " . $args->other->getName(); });

        $eventRegistry->registerHandler(array("handledEvent" => "greet", "priority" => 99, "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Human_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Cat_"))),
                function ($args) { return "Hello cat " . $args->other->getName(); });


        $cat = new EventBasedClass($eventRegistry, null, "'_Animal_Cat_");
        $humanJohn = new EventBasedClass($eventRegistry, null, "'_Animal_Human_");
        $humanRichard = new EventBasedClass($eventRegistry, null, "'_Animal_Human_");

        $cat->setName(array("value" => "Kitty"));
        $humanJohn->setName(array("value" => "John"));
        $humanRichard->setName(array("value" => "Richard"));

        $this->assertEquals("Miau",                  $cat->greet(array("other" => $humanJohn)));
        $this->assertEquals("Hello cat Kitty", $humanJohn->greet(array("other" => $cat)));
        $this->assertEquals("Hello John",   $humanRichard->greet(array("other" => $humanJohn)));
    }


    public function testSpeed()
    {
        $eventRegistry = new EventRegistry();

        $eventRegistry->registerHandler(array("handledEvent" => "greet", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Cat_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Human_"))),
                function () {  });

        $cat = new EventBasedClass($eventRegistry, null, "'_Animal_Cat_");
        $human = new EventBasedClass($eventRegistry, null, "'_Animal_Human_");

        for ($i = 0; $i < 10000; $i++)
            $cat->greet(array("other" => $human));
    }
}
