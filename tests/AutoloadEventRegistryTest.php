<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

class AutoloadEventRegistryTest extends \PHPUnit_Framework_TestCase
{
    public function test()
    {
        $registry = new AutoloadEventRegistry(new EventRegistry());
        $registry->registerHandler(array("handledEvent" => "foo"), new Handler\ClassHandler("\Nunzion\AutoloadEventRegistryTest\TestClass",
                new Handler\SourceCodeString("namespace Nunzion\AutoloadEventRegistryTest; class TestClass {}")));

        $object = new \Nunzion\AutoloadEventRegistryTest\TestClass();
        $this->assertNotNull($object);
    }
}
