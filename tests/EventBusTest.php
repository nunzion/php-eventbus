<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus;

class EventBusTest extends \PHPUnit_Framework_TestCase
{
    public function testEventBus()
    {
        $eventRegistry = new EventRegistry();


        $eventRegistry->registerHandler(array("handledEvent" => "setName", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Animal_"))),
                function ($args) { $args->self->data->_name = $args->value; });

        $eventRegistry->registerHandler(array("handledEvent" => "getName", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Animal_"))),
                function ($args) { return $args->self->data->_name; });



        $eventRegistry->registerHandler(array("handledEvent" => "greet", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Cat_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Human_"))),
                function () { return "Miau"; });

        $eventRegistry->registerHandler(array("handledEvent" => "greet", "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Human_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Animal_"))),
                function ($args) { return "Hello " . $args->other->getName(); });

        $eventRegistry->registerHandler(array("handledEvent" => "greet", "priority" => 99, "constraints" => array(
                      EventBasedClass::getClassParameterConstraint("self", "_Human_"),
                      EventBasedClass::getClassParameterConstraint("other", "_Cat_"))),
                function ($args) { return "Hello cat " . $args->other->getName(); });

        $eventBus = new EventBus($eventRegistry);


        $cat = new EventBasedClass($eventRegistry, null, "'_Animal_Cat_");
        $humanJohn = new EventBasedClass($eventRegistry, null, "'_Animal_Human_");
        $humanRichard = new EventBasedClass($eventRegistry, null, "'_Animal_Human_");

        $cat->setName(array("value" => "Kitty"));
        $humanJohn->setName(array("value" => "John"));
        $humanRichard->setName(array("value" => "Richard"));

        $this->assertEquals("Miau",
                $eventBus->greet(array("self" => $cat, "other" => $humanJohn)));
        $this->assertEquals("Hello cat Kitty",
                $eventBus->greet(array("self" => $humanJohn, "other" => $cat)));
        $this->assertEquals("Hello John",
                $eventBus->greet(array("self" => $humanRichard, "other" => $humanJohn)));
    }
}
