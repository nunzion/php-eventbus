<?php

/**
 * @author Henning Dieterichs <henning.dieterichs@hediet.de>
 * @copyright 2013 Henning Dieterichs <henning.dieterichs@hediet.de>
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace Nunzion\EventBus\Handler;

class HandlerTest extends \PHPUnit_Framework_TestCase
{
    public function testClassHandler()
    {
        $classHandler = new ClassHandler("Nunzion\EventBus\Handler\DummyClass");
        $classHandler->setAdditionalArgs((object)array("foo" => "bar"));

        $chain = new HandlerChain(array());
        $result = $classHandler((object)array("bla" => "blubb"), $chain);

        $this->assertInstanceOf("Nunzion\EventBus\Handler\DummyClass", $result);
        $this->assertEquals((object)array("foo" => "bar", "bla" => "blubb"), $result->args);
        $this->assertSame($chain, $result->handlerChain);
    }

    public function testFunctionHandler()
    {
        $functionHandler = new FunctionHandler("Nunzion\EventBus\Handler\DummyFunction");
        $functionHandler->setAdditionalArgs((object)array("foo" => "bar"));

        $chain = new HandlerChain(array());
        $result = $functionHandler((object)array("bla" => "blubb"), $chain);

        $this->assertEquals(array("args" =>
            (object)array("foo" => "bar", "bla" => "blubb"), "handlerChain" => $chain), $result);
    }

    public function testSourceCodeString()
    {
        $functionHandler = new FunctionHandler("test123", 
                new SourceCodeString("function test123() { return 'foo'; }"));
        $this->assertEquals("foo", $functionHandler((object)array(), null));
        $this->assertEquals("foo", $functionHandler((object)array(), null));
    }
}

function DummyFunction($args, $handlerChain)
{
    return array("args" => $args, "handlerChain" => $handlerChain);
}

class DummyClass
{
    public $args;
    public $handlerChain;

    public function __construct($args, $handlerChain)
    {
        $this->args = $args;
        $this->handlerChain = $handlerChain;
    }
}
